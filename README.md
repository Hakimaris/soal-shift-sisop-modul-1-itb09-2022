# soal-shift-sisop-modul-1-ITB09-2022

Berikut Merupakan Hasil Pengerjaan Soal Shift Praktikum SISTEM OPERASI 2022 kelompok ITB09<br>
Anggota Kelompok:<br>

<ol>
<li>Fatih Rian Hibatul Hakim (5027201066)</li>
<li>Naufal Dhiya Ulhaq(50272010xx)</li>
<li>Kevin Oktoaria (50272010xx)</li>
</ol>

---

# Soal 1

## Soal 1a

### Analisa Soal

Pada soal ini, kami diminta untuk membuat program login ```main.sh```, program register ```register.sh```, dan file ```user.txt``` untuk menyimpan data username dan password.

### Cara Pengerjaan

### Main.sh

Pada main.sh, kami menggunakan if else dimana pertama-tama user menginputkan username dan password. Disini terdapat command read dimana berfungsi agar user dapat menginputkan username dan password. Disini juga terdapat ```-sp``` dimana berfungsi untuk menyembunyikan input password di terminal. Lalu kami menggunakan awk pada ```main.sh```, ini berfungsi untuk mencari data username dan password pada file ```user.txt```, dimana pada awk ini terdapat ```-v``` yang berarti variable dan ```$3==username && $7==password``` yang berarti pada file user.txt username terdapat pada field ke-3 sedangkan password pada field ke-7.

```c
#!/bin/bash

printf "<<WELCOME TO LOGIN SITE>>\n"	
##INPUT USERNAME & PASSWORD
read -p "Username = " username
read -sp "Password = " password
printf "\n"
time=`date +%m/%d/%Y`
hour=`date +%H:%M:%S`
check=$(awk -v username="${username}" -v password="${password}" '$3==username && $7==password {print 1}' users/user.txt)

function afterlogin(){
    printf "What do you want to do? \n"
    printf "dl N = download picture N times\n"
    printf "att = count how many login attempt you've done\n"
    printf "\n"
    read command n
##dl
    if [ $command == 'dl' ]
    then
	time2=`date +%Y-%m-%d_`
	printf $time2
	dir="$time2$username"
	if zipfile=$(ls | grep -o ".*${username}.zip")
	then
		unzip $zipfile
		dir=${zipfile::-4}
	else
		dir="$time2$username"
		mkdir $dir
	fi
	count=$(ls $dir |  grep -c "PIC")
	for ((i = count + 1; i <= count + n; i = i + 1))
	do
		wget -O ${dir}/PIC_0${i} https://loremflickr.com/320/240
	done
		newdir="$time2$username"
		zip -r ${newdir}.zip $dir
		rm -r $dir
		printf "## DOWNLOAD SUCCESFUL ##\n"
##attempt
    elif [ $command == 'att' ]
    then
	fail=$(grep -c "LOGIN: ERROR Failed login attempt on user $username" log.txt)
	success=$(grep -c "LOGIN: INFO User $username logged in" log.txt)
	count=$(($fail + $success))
        printf "## LOGIN ATTEMPT FOR USER $username IS $count TIMES ##\n"
    else
        printf "## INPUT CAN'T BE EMPTY ##\n"
    fi
}


##Pengecekan apakah username dan password benar atau salah
if [ "$check" == 1 ]
then
    printf "## LOGIN SUCCESSFULLY ##\n"
    printf "\n$time $hour LOGIN: INFO User $username logged in" >> log.txt
    afterlogin
else
    while [[ "$check" != 1 ]]
    do
        printf "\n$time LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    	printf "## WRONG USERNAME OR PASSWORD ##\n"
    	read -p "Username = " username
	read -sp "Password = " password
	printf "\n"
    	check=$(awk -v username="${username}" -v password="${password}" '$3==username && $7==password {print 1}' users/user.txt)
    done
        printf "## LOGIN SUCCESSFULLY ##\n"
    	printf "\n$time $hour LOGIN: INFO User $username logged in" >> log.txt
    	afterlogin
fi
```

### Register.sh

Pada register.sh, kami menggunakan if else. Pada register.sh terdapat [-sp] dimana berfungsi untuk menyembunyikan input password di terminal.

```c
#!/bin/bash

printf "<<WELCOME TO REGISTER SITE>>\n"

#USERNAME
read -p "Username = " username
time=`date +%m/%d/%Y`
hour=`date +%H:%M:%S`
#Username sudah terpakai
if egrep "$username" users/user.txt > users/temp.txt
then
	printf "## $username ALREADY EXISTS ##\n"
	printf "\n$time $hour REGISTER: Error User already exists" >> log.txt
	exit 1
fi

rm users/temp.txt

#PASSWORD
read -sp "Password = " password
printf "\n"
#Password minimal 8 karakter
if [ ${#password} -lt 8 ]
then
	printf "## PASSWORD HAS LESS THAN 8 CHARACTERS ##\n"
#Huruf kapital dan huruf kecil
elif [ $password = ${password,,} ]
then
	printf "## PASSWORD DOESN'T HAVE UPPERCASE LETTER ##\n"
elif [ $password = ${password^^} ]
then
	printf "## PASSWORD DOESN'T HAVE LOWERCASE LETTER ##\n"
##Password tidak memiliki alphanumeric
elif [[ $password != *[0-9]* ]]
then
	printf "## PASSWORD DOESN'T HAVE ALPHANUMERIC ##\n"
#Password sama dengan username
elif [ $password == $username ]
then
	printf "## PASSWORD IS THE SAME AS YOUR USERNAME ##\n"
#Register berhasil
else
	printf "## USER HAS BEEN ADDED SUCCESSFULLY ##\n"
	printf "\nUsername : $username || Password : $password" >> users/user.txt
	printf "\n$time $hour REGISTER: INFO User $username registered successfully" >> log.txt
fi
```


## Soal 1b

### Analisa Soal

Pada soal ini, kami diminta untuk membuat fungsi untuk kriteria password pada [register.sh] dimana kriterianya adalah sebagai berikut:

1. Minimal 8 karakter
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
3. Alphanumeric
4. Tidak boleh sama dengan username

### Cara Pengerjaan

Pada soal ini, kelompok kami menggunakan if else untuk pengecekkan kriteria

```c
#PASSWORD
read -sp "Password = " password
printf "\n"
#Password minimal 8 karakter
if [ ${#password} -lt 8 ]
then
	printf "## PASSWORD HAS LESS THAN 8 CHARACTERS ##\n"
#Huruf kapital dan huruf kecil
elif [ $password = ${password,,} ]
then
	printf "## PASSWORD DOESN'T HAVE UPPERCASE LETTER ##\n"
elif [ $password = ${password^^} ]
then
	printf "## PASSWORD DOESN'T HAVE LOWERCASE LETTER ##\n"
##Password tidak memiliki alphanumeric
elif [[ $password != *[0-9]* ]]
then
	printf "## PASSWORD DOESN'T HAVE ALPHANUMERIC ##\n"
#Password sama dengan username
elif [ $password == $username ]
then
	printf "## PASSWORD IS THE SAME AS YOUR USERNAME ##\n"
#Register berhasil
else
	printf "## USER HAS BEEN ADDED SUCCESSFULLY ##\n"
	printf "\nUsername : $username || Password : $password" >> users/user.txt
	printf "\n$time $hour REGISTER: INFO User $username registered successfully" >> log.txt
fi
```

### Kendala

Sebelum menemukan cara untuk alphanumeric, kami mengalami error berkali-kali tetapi sekarang sudah berhasil.

## Soal 1c

### Analisa Soal

Pada soal ini, kami diminta untuk merecord segala aktivitas seperti login successful, login gagal, register successful, dan username sudah dipakai. Segala aktivitas tersebut akan tersimpan pada log.txt

### Cara Pengerjaan

Disini kelompok kami menggunakan printf agar lebih mudah menggunakan [\n]
Untuk main.sh, codenya adalah sebagai berikut:

```c
printf "\n$time $hour LOGIN: INFO User $username logged in" >> log.txt
printf "\n$time $hour LOGIN: ERROR Failed login attempt on user $username" >> log.txt

```


Untuk register.sh, codenya adalah sebagai berikut:

```c
printf "\n$time $hour REGISTER: INFO User $username registered successfully" >> log.txt
printf "\n$time $hour REGISTER: Error User already exists" >> log.txt
```


## Soal 1d

### Analisa Soal

Pada soal ini, kami diminta untuk membuat fungsi dalam main.sh. Dimana terdapat 2 yaitu dl N yang berfungsi untuk mendownload gambar sebanyak N kali, dan att yang berfungsi untuk mengecek berapa kali percobaan atau attempt login yang telah dilakukan user

### Cara Pengerjaan

Disini kelompok kami menggunakan menggunakan fungsi afterlogin dimana pada fungsi ini user dapat melakukan 2 hal yaitu att atau dl N, codenya adalah sebagai berikut:

```c
function afterlogin(){
    printf "What do you want to do? \n"
    printf "dl N = download picture N times\n"
    printf "att = count how many login attempt you've done\n"
    printf "\n"
    read command n
##dl
    if [ $command == 'dl' ]
    then
	time2=`date +%Y-%m-%d_`
	printf $time2
	dir="$time2$username"
	if zipfile=$(ls | grep -o ".*${username}.zip")
	then
		unzip $zipfile
		dir=${zipfile::-4}
	else
		dir="$time2$username"
		mkdir $dir
	fi
	count=$(ls $dir |  grep -c "PIC")
	for ((i = count + 1; i <= count + n; i = i + 1))
	do
		wget -O ${dir}/PIC_0${i} https://loremflickr.com/320/240
	done
		newdir="$time2$username"
		zip -r ${newdir}.zip $dir
		rm -r $dir
		printf "## DOWNLOAD SUCCESFUL ##\n"
##attempt
    elif [ $command == 'att' ]
    then
	fail=$(grep -c "LOGIN: ERROR Failed login attempt on user $username" log.txt)
	success=$(grep -c "LOGIN: INFO User $username logged in" log.txt)
	count=$(($fail + $success))
        printf "## LOGIN ATTEMPT FOR USER $username IS $count TIMES ##\n"
    else
        printf "## INPUT CAN'T BE EMPTY ##\n"
    fi
}
```

## Output:
![Fungsi dl](https://cdn.discordapp.com/attachments/909847811745525801/949681509114200104/Screenshot_443.png)
![Fungsi att](https://cdn.discordapp.com/attachments/909847811745525801/949681509399425094/Screenshot_442.png)

---
# Soal 2
Pada soal ini, peserta diminta untuk membuat folder bernama "forensic_log_website_daffainfo_log." dimana isi dari folder ini merupakan file bernama ```ratarata.txt```rata-rata serangan kepada website daffainfo serta file bernama ```result.txt``` yang berisikan IP terbanyak, berapa request yang menggunakan user-agent curl, dan daftar IP yang mengakses https://daffainfo. untuk mengerjakan soal ini disediakan file log dari serangan yang dilakukan pada https://daffainfo.

## Soal 2a
### Analisa soal
Pada soal ini diminta untuk membuat folder bernama "forensic_log_website_daffainfo_log.". Untuk mengerjakan soal ini pendeketana yang saya ambil yaitu dengan menghapus folder dengan nama yaang sama, lalu baru dibuatkan folder dengan nama yang sudah disesuaikan.

### Penyelesaian
untuk penyelesaian soal ini bisa menggunakan command ```mkdir``` untuk membuat folder direktori serta ```rm``` untuk menghapus direktori yang ada. 

```c
rm -f -r forensic_log_website_daffainfo_log.
mkdir -p forensic_log_website_daffainfo_log.
```
pada bagian ```rm``` ada beberapa opsi ```-f``` serta ```-r```. ```-f``` bermakna *force*  yang akan memaksa folder untuk dihapus. sedangkan opsi ```-r``` bermakna *rekursive* dimana rekursif bermankna hapus untuk beserta konten direktori

## Soal 2b
### Analisa soal
Pada soal ini peserta diminta untuk menghitung rata-rata serangan pada web yang diserang dalam per jam. pendekatan yang saya gunakan hanya dengan membagi seluruh log serangan (terkecuali header dari log) lalu dibagi 12 karena serangan berakhir pada jam 12.

### Penyelesaian
untuk pennyelesaian soal ini bisa diselesaikan dengan AWK serta syntax bernama ```NR``` dimana bermakna berapa baris yang telah dilalui AWK. apabila sudah dilalui semua maka kurangi 1 karena ada satu baris didalam log yang bukan merupakan report dari serangan. setelah itu baru dibagi 12. apabila sudah dikerjakan, masukan hasil ke file yang diarahkan.

```c
awk 'END {printf "Rata-rata serangan adalah sebanyak %f requests per jam", (NR-1)/12}
' log_website_daffainfo.log > forensic_log_website_daffainfo_log./ratarata.txt
```

## Soal 2c
### Analisa soal
pada soal ini diminta untuk menampilak IP yang paling sering menyerang website daffainfo. 

### Penyelesaian
pendekatan yang saya gunakan iyalah membuat function awk lalu memasukan kedalam array serta melakukan increment.

```c
awk 'function yuhu()
    {aipi[$1]++} 
    END {for (i in aipi) printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests \n\n\n",i,aipi[i]}

    BEGIN{FS=":"}{yuhu()}
' log_website_daffainfo.log | sort -r -nk10 | head -1 > forensic_log_website_daffainfo_log./result.txt
```
Untuk menampilkan serangan terbanyak bisadilakukan dengan command ```sort``` dan ```head```.
```sort``` memiliki opsi ```-r``` untuk mereverse hasil loop serta membuat count tertinggi tampil ke atas serta ```-nk10``` untuk mensort berdasarkan kata ke 10.

## Soal 2d
### Analisa soal
Pada soal ini diminta untuk mencari berapa serangan dengan user agent curl

### Penyelesaian
untuk menyelesaikan ini bisa menggunakan awk serta fungsi ```[/kata yang mau dicari/] ``` 

```c
awk '/curl/ {++n} 
END {printf "\nAda "n" requests yang menggunakan curl sebagai user-agent\n\n"}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log./result.txt
```
apabila sudah selesai, masukain hasil kepada file *result.txt*

## Soal 2e
### Analisa soal
pada soal ini diminta untuk mencari serangan yang dilakukan pada jam 2 pagi tanggal 22.

### Penyelesaian
untuk penyelesaian soal ini bisa menggunakan command ```mkdir``` untuk membuat folder direktori serta ```rm``` untuk menghapus direktori yang ada.

```c
awk '
    BEGIN{FS=":"}
    {if($3=="02") printf "%s\n", $1}
' log_website_daffainfo.log | sort -r >> forensic_log_website_daffainfo_log./result.txt
```
## output:
![result](https://cdn.discordapp.com/attachments/758864655828779029/950048634857226311/unknown.png)
![ratarata](https://cdn.discordapp.com/attachments/758864655828779029/950048444968484974/unknown.png)

---
# Soal 3
Pada soal ini kami diminta untuk memonitoring ram dan size dari suatu directory. Untuk memonitoring ram, kami dapat menggunakan command free -m. Sedangkan untuk memonitoring size, kami dapat menggunakan command du -sh dengan target pada directory /home/user/.

## Soal 3a

### Analisa Soal
Pada soal ini, kami diminta untuk memasukkan hasil data monitoring pada sebuah file dengan format nama ```metrics_{YmdHms}.log```.

### penyelesaian
Untuk mendapatkan format nama yang sesuai, kami dapat menggunakan command “date” dengan menyesuaikan format yang telah diberikan. Kemudian hasil dari command “date” tersebut dapat disimpan dalam sebuah variabel bernama $tanggal. Selain itu kami juga membuat sebuah variabel bernama $namafile yang berisi format pernamaan secara lengkap.

```c
namauser=$(whoami)
tanggal=$(date +'%Y%m%d%H%M%S')
namafile="metrics_$tanggal"

mkdir -p /home/$namauser/log

Selanjutnya, untuk mendaptkan data log yang diminta, kami menggunakan command awk. Pada monitoring ram kami menggunakan free -m sebagai input. Sedangkan pada monitoring size, kami menggunakan du -sh sebagai inputnya.

printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n" > /home/$namauser/log/$namafile.log
awk '/Mem/ {printf "%u,%u,%u,%u,%u,%u,",$2,$3,$4,$5,$6,$7}
' <(free -m) >> /home/$namauser/log/$namafile.log
awk '/Swap/{printf "%u,%u,%u,",$2,$3,$4}
' <(free -m) >> /home/$namauser/log/$namafile.log
awk '{printf "%s,%s",$2,$1}
' <(du -sh /home/naufal/) >> /home/$namauser/log/$namafile.log
```

## Soal 3b

### Analisa Soal
Pada soal ini, kami diminta untuk membua script untuk pemubatan file log tadi berjalan otomatis setiap menit. Hal ini dapat dilakukan dengan menggunakan cronjob.

### Penyelesaian
Untuk menerapkan cronjob, kami memasukkan line baru pada file crontab. Karena kami diminta untuk melakukan otomasi setiap menit, maka kami memasukkan line baru seperti berikut:

```* * * * * /bin/bash /home/naufal/Sisop/minute_log.sh```

```* * * * * -> berarti otomasi dilakukan tiap menit.```

/bin/bash /home/naufal/Sisop/minute_log.sh  berarti mengeksekusi script pada path directory tersebut.

## Soal 3c

### Analisa Soal
Pada soal ini, kami diminta untuk membuat script yang dapat mengenerate file yang berisi aggregate dari file-file log yang tergenerate 1 jam sebelumnya. Script ini diharapkan berjalan secara otomatis pada tiap jam dan mengenerate file log baru dengan format nama metrics_agg_{YmdH}.log.

### Penyelesaian
Untuk mendapatkan data log dari file-file log yang telah tergenerarte 1 jam terakhir, kita dapat menggunakan awk dengan multifile input. Hal ini dapat dilakukan dengan seperti berikut:

```c
awk 'function memuse()
    {if(NR==2) print $2}
    BEGIN{FS=","}{memuse()}
' /home/$namauser/log/$namafile*.log | sort -nk1 >> /home/$namauser/log/$namafile.log
```

Setelah mendaptkan data-data tersebut, seharusnya kita dapat mensorting data-data tersebut sehingga dapat menemukan value minimum dan maximumnya. Akan tetapi, sangat disayangkan, hingga akhir waktu pengerjaan saya belum dapat menemukan cara yang tepat untuk mensorting data-data tersebut.

Selain itu, untuk mengenerate file log dengan format nama yang ditentukan, kami menggunakan cara yang sama dengan script sebelumnya. Kami menggunakan command “date” dan dimasukkan ke dalam sebuah variabel bernama $tanggal. Kemudia untuk nama file scara utuhnya kami menggunakan variabel bernama $namafile yang berisi sebagai berikut:

```c
namauser=$(whoami)
tanggal=$(date +'%Y%m%d%H')
namafile="metric_agg_$tanggal"
```

Kemudian untuk membuat script berjalan secara otomatis pada tiap jam, kami menggunakan cronjob. Kami menambahkan line baru pada crontab yang berisi sebagai berikut:

```c
0 * * * * /bin/bash /home/naufal/Sisop/aggregate_minutes_to_hourly_log.sh

0 * * * * -> berarti otomasi dilakukan tiap menit 0.
/bin/bash /home/naufal/Sisop/aggregate_minutes_to_hourly_log.sh  berarti mengeksekusi script pada path directory tersebut.
```

Untuk mengumpulkan data-data log dari log-log yang sudah tergenerate sebelumnya, kami membuat sebuah function yang bernama loglist sebagai berikut:
```c
function logslist(){
for logs in $(ls /home/$namauser/log/metrics_$tanggal*.log); do cat $logs | awk '$0 !~ /mem/'; done}

Selanjutnya kami menggunakan AWK untuk menemukan nilai maksimal, minimal, serta rata-rata dari tiap data tersebut dan kemudian mengassign nya ke variabel dengan nama yang sesuai.
Contoh kode:
swap_free_sorting=$(logslist | awk -F, '
{
    if(min==""){min=max=$9}; 
    if($9>max) {max=$9};
    if($9<min) {min=$9};
    total+=$9; count+=1
} 
END {
    print min,max,total/count
}')

swap_free_min=$(echo $swap_free_sorting | awk '{print $1}' | tr ',' '.')
swap_free_max=$(echo $swap_free_sorting | awk '{print $2}' | tr ',' '.')
swap_free_avg=$(echo $swap_free_sorting | awk '{print $3}' | tr ',' '.')
```

Terakhir, untuk memasukkan data-data tersebut ke dalam file, kami menggunakan command echo dengan output ke file yang namanya sudah disesuaikan dengan format yang dimminta.
```c
echo "minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$location,$path_size_min" >> /home/$namauser/log/$namafile.log
echo "maximum,$mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$location,$path_size_max" >> /home/$namauser/log/$namafile.log
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$location,$path_size_avg" >> /home/$namauser/log/$namafile.log
```

## Soal 3d

## Analisa Soal
Pada soal ini, kami diminta untuk memastikan bahwa file-file log yang telah tergenerate hanya dapat dibaca oleh user pemilik file saja.

## Penyelesaian
Kami menggunakan command “chmod 700” pada akhir script untuk mengubah permission pada file-file yang telah tergenerate.
```chmod 700 /home/$namauser/log/$namafile.log ```

## output :
![hasilcron](https://cdn.discordapp.com/attachments/903636835127918694/949928393305116712/unknown.png)
![minutlog](https://cdn.discordapp.com/attachments/903636835127918694/949928574381596672/unknown.png)
![isiagregat](https://cdn.discordapp.com/attachments/903636835127918694/949928664156495892/unknown.png)