#!/bin/bash

printf "<<WELCOME TO LOGIN SITE>>\n"
##INPUT USERNAME & PASSWORD
read -p "Username = " username
read -sp "Password = " password
printf "\n"
time=`date +%m/%d/%Y`
hour=`date +%H:%M:%S`
check=$(awk -v username="${username}" -v password="${password}" '$3==username && $7==password {print 1}' users/user.txt)

function afterlogin(){
    printf "What do you want to do? \n"
    printf "dl N = download picture N times\n"
    printf "att = count how many login attempt you've done\n"
    printf "\n"
    read command n
##dl
    if [ $command == 'dl' ]
    then
	time2=`date +%Y-%m-%d_`
	printf $time2
	dir="$time2$username"
	if zipfile=$(ls | grep -o ".*${username}.zip")
	then
		unzip $zipfile
		dir=${zipfile::-4}
	else
		dir="$time2$username"
		mkdir $dir
	fi
	count=$(ls $dir |  grep -c "PIC") 
	for ((i = count + 1; i <= count + n; i = i + 1))
	do
		wget -O ${dir}/PIC_0${i} https://loremflickr.com/320/240
	done
		newdir="$time2$username"
		zip -r ${newdir}.zip $dir
		rm -r $dir
		printf "## DOWNLOAD SUCCESFUL ##\n"
##attempt
    elif [ $command == 'att' ]
    then
	fail=$(grep -c "LOGIN: ERROR Failed login attempt on user $username" log.txt)
	success=$(grep -c "LOGIN: INFO User $username logged in" log.txt)
	count=$(($fail + $success))
        printf "## LOGIN ATTEMPT FOR USER $username IS $count TIMES ##\n"
    else
        printf "## INPUT CAN'T BE EMPTY ##\n"
    fi
}


##Pengecekan apakah username dan password benar atau salah
if [ "$check" == 1 ]
then
    printf "## LOGIN SUCCESSFULLY ##\n"
    printf "\n$time $hour LOGIN: INFO User $username logged in" >> log.txt
    afterlogin
else
    while [[ "$check" != 1 ]]
    do
        printf "\n$time $hour LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    	printf "## WRONG USERNAME OR PASSWORD ##\n"
    	read -p "Username = " username
	read -sp "Password = " password
	printf "\n"
    	check=$(awk -v username="${username}" -v password="${password}" '$3==username && $7==password {print 1}' users/user.txt)
    done
        printf "## LOGIN SUCCESSFULLY ##\n"
    	printf "\n$time $hour LOGIN: INFO User $username logged in" >> log.txt
    	afterlogin
fi