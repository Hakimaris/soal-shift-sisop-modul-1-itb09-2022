#!/bin/bash

printf "<<WELCOME TO REGISTER SITE>>\n"

#USERNAME
read -p "Username = " username
time=`date +%m/%d/%Y`
hour=`date +%H:%M:%S`
#Username sudah terpakai
if egrep "$username" users/user.txt > users/temp.txt
then 
	printf "## $username ALREADY EXISTS ##\n"
	printf "\n$time $hour REGISTER: Error User already exists" >> log.txt
	exit 1
fi

rm users/temp.txt

#PASSWORD
read -sp "Password = " password
printf "\n"
#Password minimal 8 karakter
if [ ${#password} -lt 8 ] 
then
	printf "## PASSWORD HAS LESS THAN 8 CHARACTERS ##\n"
#Huruf kapital dan huruf kecil
elif [ $password = ${password,,} ] 
then
	printf "## PASSWORD DOESN'T HAVE UPPERCASE LETTER ##\n"
elif [ $password = ${password^^} ] 
then
	printf "## PASSWORD DOESN'T HAVE LOWERCASE LETTER ##\n"
##Password tidak memiliki alphanumeric
elif [[ $password != *[0-9]* ]]
then
	printf "## PASSWORD DOESN'T HAVE ALPHANUMERIC ##\n"
#Password sama dengan username
elif [ $password == $username ] 
then
	printf "## PASSWORD IS THE SAME AS YOUR USERNAME ##\n"
#Register berhasil
else
	printf "## USER HAS BEEN ADDED SUCCESSFULLY ##\n"
	printf "\nUsername : $username || Password : $password" >> users/user.txt
	printf "\n$time $hour REGISTER: INFO User $username registered successfully" >> log.txt
fi