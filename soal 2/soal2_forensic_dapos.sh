#!/bin/bash

#A. bikin folder, hapus folder beserta isinya kalo sudah ada
rm -f -r forensic_log_website_daffainfo_log.
mkdir -p forensic_log_website_daffainfo_log.

#B. rata2
awk 'END {printf "Rata-rata serangan adalah sebanyak %f requests per jam", (NR-1)/12}
' log_website_daffainfo.log > forensic_log_website_daffainfo_log./ratarata.txt

##cara ribed dan gagal
# awk 'function yuhud()
#     {NR==2}
#     {jam[$3]++} {menit[$4]++} {detik[$5]++}
#     {for (i in jam) jamber+=jam[i]}
#     {for (i in menit) menitber+=menit[i]}
#     {for (i in detik) detikber+=detik[i]}
#     END {printf "%f %f %f",jamber,menitber,detikber}

#     BEGIN{FS=":"}{yuhud()}
# ' log_website_daffainfo.log > forensic_log_website_daffainfo_log./rsss.txt

# C. ip muncul terbanyak
awk 'function yuhu()
    {aipi[$1]++} 
    END {for (i in aipi) printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests \n\n\n",i,aipi[i]}

    BEGIN{FS=":"}{yuhu()}
' log_website_daffainfo.log | sort -r -nk10 | head -1 > forensic_log_website_daffainfo_log./result.txt

#D. ada brp request yg make curl
awk '/curl/ {++n} 
END {printf "\nAda "n" requests yang menggunakan curl sebagai user-agent\n\n"}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log./result.txt

#E. Pada jam 2 pagi pada tanggal 22, ip apa saja yang mengakses
awk '
    BEGIN{FS=":"}
    {if($3=="02") printf "%s\n", $1}
' log_website_daffainfo.log | sort -r >> forensic_log_website_daffainfo_log./result.txt