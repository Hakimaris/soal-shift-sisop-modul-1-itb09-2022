#!/bin/bash

mkdir -p /home/naufal/log

namauser=$(whoami)
tanggal=$(date +'%Y%m%d%H%M%S')
namafile="metrics_$tanggal"

printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size\n" > /home/$namauser/log/$namafile.log
awk '/Mem/ {printf "%u,%u,%u,%u,%u,%u,",$2,$3,$4,$5,$6,$7}
' <(free -m) >> /home/$namauser/log/$namafile.log
awk '/Swap/{printf "%u,%u,%u,",$2,$3,$4}
' <(free -m) >> /home/$namauser/log/$namafile.log
awk '{printf "%s,%s",$2,$1}
' <(du -sh /home/naufal/) >> /home/$namauser/log/$namafile.log